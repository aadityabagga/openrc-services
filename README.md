A set of OpenRC services, fetched from upstream and modifed as needed.

Currently these can be used on Slackware Linux.

### Links

* Slackbuilds: https://slackbuilds.org/repository/14.2/system/openrc-services/

* Documentation: http://docs.slackware.com/howtos:general_admin:openrc

* Upstream: https://github.com/gentoo/gentoo

### Note

See [Hacking.md](HACKING.md) for setup details.
