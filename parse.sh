#!/bin/bash
# parse.sh: parses the log generated by download.sh and displays updated packages with date of update and package category

# Copyright (C) 2015 Aaditya Bagga <aaditya_gnulinux@zoho.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

# Name of the log file
log="download.log"

# Change to the logs directory
cd logs

# Specify the package categories
OPENRC_AUR=(teamviewer)
OPENRC_BASE=(cronie cryptsetup dbus glibc kmod lvm2 mdadm dhcpcd eudev)
OPENRC_DESKTOP=(acpid alsa-utils bluez cgmanager consolekit gpm wpa_supplicant xinit xorg-server)
OPENRC_DEVEL=(git mysql-init-scripts php postgresql subversion)
OPENRC_MISC=(anacron at bitlbee boinc clamav connman cpupower cups fcron fuse haveged hdparm libvirt lirc lm_sensors metalog mpd ntp pulseaudio rsyslog salt sane-backends syslog-ng thermald virtualbox-guest-additions xe-guest-utilities)
OPENRC_NET=(autofs bind cyrus-sasl dhcp hostapd iptables lighttpd mit-krb5 networkmanager nfs-utils nginx openldap openntpd openslp openssh openvpn polipo postfix privoxy quota rpcbind rsync samba squid tor transmission ufw vsftpd vnstat xinetd ypbind ypserv)
OPENRC_SLACK=(dcron proftpd net-snmp sysklogd sendmail ulogd)
OPENRC_VIDEO=(ati-drivers bumblebee nvidia-drivers virtualgl)

check_package () {
 	package="$1" # package to be searched is first argument
	for p in OPENRC_AUR OPENRC_BASE OPENRC_DESKTOP OPENRC_DEVEL OPENRC_MISC OPENRC_NET OPENRC_SLACK OPENRC_VIDEO; do
		# Get the packages in each group
		eval "j=\${$p[*]}"
		for k in $j; do
			if [ "$package" = "$k" ]; then
				echo -e "\t$p"
			fi
		done
	done
}

# Get lines which start with a date
lines=($(grep -e "[0-9]*-[0-9]*-[0-9]*_[0-9]*:[0-9]*:[0-9]*" -n $log | cut -f 1 -d ":"))
# End of file also qualifies
lines[${#lines[*]}]=$(wc -l $log | cut -f 1 -d " ")

# Get the dates too
dates=($(grep -e "[0-9]*-[0-9]*-[0-9]*_[0-9]*:[0-9]*:[0-9]*" $log))

# If there are 5 lines, loop has to run 4 times
let loop=${#lines[*]}-1

# Parse the log file in reverse (bottom to up), to show recent updates first
for ((i=loop;i>0;i--)); do
	lastline=${lines[$i]}
	firstline=${lines[$i-1]}
	# Get the updated packages between those lines, which are NOT patches
	updated=($(sed -ne "${firstline},${lastline}p;${lastline}q" < ${log} | grep -v ".patch" | grep -v ".service" | grep "^U" | grep -e "init" -e "conf.d" -e "confd" | cut -f 3 -d "/" | uniq))
	for x in "${updated[@]}"; do
		# Print the package and date of updation
		printf "%-25s %s\t" "$x" "${dates[$i-1]}" # dates is -1 as last line (EOF) is not a date
		# Print package group
		check_package "$x"
	done
done

exit $?
