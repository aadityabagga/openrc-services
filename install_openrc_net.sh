#!/bin/sh
# install_openrc_net.sh

DESTDIR=$1

_gentoo_uri="gentoo-x86"
_apver=2.4.18-r1

# files
_Cbind=net-dns/bind/files/named.confd-r7
_Ibind=net-dns/bind/files/named.init-r13
_Csasl=dev-libs/cyrus-sasl/files/saslauthd-2.1.26.conf
_Isasl=dev-libs/cyrus-sasl/files/saslauthd2.rc7
_Cdhcp1=net-misc/dhcp/files/dhcpd.conf2
_Cdhcp2=net-misc/dhcp/files/dhcrelay.conf
_Cdhcp3=net-misc/dhcp/files/dhcrelay6.conf
_Idhcp1=net-misc/dhcp/files/dhcpd.init5
_Idhcp2=net-misc/dhcp/files/dhcrelay.init3
_Chost=net-wireless/hostapd/files/hostapd-conf.d
_Ihost=net-wireless/hostapd/files/hostapd-init.d
_Cip1=net-firewall/iptables/files/iptables-1.4.13.confd
_Cip2=net-firewall/iptables/files/ip6tables-1.4.13.confd
_Iip=net-firewall/iptables/files/iptables-1.4.13-r1.init
#_Ikrb1=app-crypt/mit-krb5/files/mit-krb5kadmind.initd-r1
#_Ikrb2=app-crypt/mit-krb5/files/mit-krb5kdc.initd-r1
#_Ikrb3=app-crypt/mit-krb5/files/mit-krb5kpropd.initd-r1
#_Clight=www-servers/lighttpd/files/lighttpd.confd
#_Ilight=www-servers/lighttpd/files/lighttpd.initd
_Cnfs=net-fs/nfs-utils/files/nfs.confd
_Infs=net-fs/nfs-utils/files/nfs.initd
_Cnfsc=net-fs/nfs-utils/files/nfsclient.confd
_Infsc=net-fs/nfs-utils/files/nfsclient.initd
_Iidmapd=net-fs/nfs-utils/files/rpc.idmapd.initd
_Ipipefs=net-fs/nfs-utils/files/rpc.pipefs.initd
_Igssd=net-fs/nfs-utils/files/rpc.gssd.initd
_Isvcgssd=net-fs/nfs-utils/files/rpc.svcgssd.initd
_Istatd=net-fs/nfs-utils/files/rpc.statd.initd
_Cldap=net-nds/openldap/files/slapd-confd-2.4.28-r1
_Ildap=net-nds/openldap/files/slapd-initd-2.4.28-r1
#_Islp=net-nds/openldap/files/slpd-init
_Cssh=net-misc/openssh/files/sshd.confd
_Issh=net-misc/openssh/files/sshd.rc6.4
_Ipolip=net-proxy/polipo/files/polipo.initd-2
_Ipostfix=mail-mta/postfix/files/postfix.rc6.2.7
_Ipriv=net-proxy/privoxy/files/privoxy.initd
_Cquota=sys-fs/quota/files/quota.confd
_Iquota1=sys-fs/quota/files/quota.rc7
_Iquota2=sys-fs/quota/files/rpc.rquotad.initd
_Crpc=net-nds/rpcbind/files/rpcbind.confd
_Irpc=net-nds/rpcbind/files/rpcbind.initd
_Crsync=net-misc/rsync/files/rsyncd.conf.d
_Irsync=net-misc/rsync/files/rsyncd.init.d-r1
_Csmb=net-fs/samba/files/4.2/samba4.confd
_Ismb=net-fs/samba/files/4.2/samba4.initd-r1
_Csquid=net-proxy/squid/files/squid.confd-r1
_Isquid=net-proxy/squid/files/squid.initd-r4
_Ctrans=net-p2p/transmission/files/transmission-daemon.confd.4
_Itrans=net-p2p/transmission/files/transmission-daemon.initd.9
_Cufw=net-firewall/ufw/files/ufw.confd
_Iufw=net-firewall/ufw/files/ufw-2.initd
_Ivsftp=net-ftp/vsftpd/files/vsftpd.init
_Svsftp=net-ftp/vsftpd/files/vsftpd-checkconfig.sh
_Iproftp=net-ftp/proftpd/files/proftpd.initd
_Cvnstat=net-analyzer/vnstat/files/vnstatd.confd
_Ivnstat=net-analyzer/vnstat/files/vnstatd.initd-r1
_Cxinet=sys-apps/xinetd/files/xinetd.confd
_Ixinet=sys-apps/xinetd/files/xinetd.rc6
_Cypbind=net-nds/ypbind/files/ypbind.confd-r1
_Iypbind=net-nds/ypbind/files/ypbind.initd
_Cypserv1=net-nds/ypserv/files/ypserv.confd
#_Cypserv2=net-nds/ypserv/files/rpc.yppasswdd.confd
#_Cypserv3=net-nds/ypserv/files/rpc.ypxfrd.confd
_Iypserv1=net-nds/ypserv/files/ypserv
#_Iypserv2=net-nds/ypserv/files/rpc.yppasswdd-r1
#_Iypserv3=net-nds/ypserv/files/rpc.ypxfrd-2.23
_Iautofs=net-fs/autofs/files/autofs5.initd
_Cvpn=net-misc/openvpn/files/openvpn-2.1.conf
_Ivpn=net-misc/openvpn/files/openvpn-2.1.init
_Contp=net-misc/openntpd/files/openntpd.conf.d-20080406-r6
_Iontp=net-misc/openntpd/files/openntpd.init.d-20080406-r6
_Ctor=net-misc/tor/files/tor.confd
_Itor=net-misc/tor/files/tor.initd-r7
_Inginx=www-servers/nginx/files/nginx.initd-r2
_CNM=net-misc/networkmanager/files/conf.d.NetworkManager
_INM=net-misc/networkmanager/files/init.d.NetworkManager
_SNM=net-misc/networkmanager/files/10-openrc-status-r4

# install
install -Dm755 "${_gentoo_uri}/${_Iautofs}" "${DESTDIR}/etc/init.d/autofs"
install -Dm644 "${_gentoo_uri}/${_Cbind}" "${DESTDIR}/etc/conf.d/named"
install -Dm755 "${_gentoo_uri}/${_Ibind}" "${DESTDIR}/etc/init.d/named"
install -Dm644 "${_gentoo_uri}/${_Csasl}" "${DESTDIR}/etc/conf.d/saslauthd"
install -Dm755 "${_gentoo_uri}/${_Isasl}" "${DESTDIR}/etc/init.d/saslauthd"
install -Dm644 "${_gentoo_uri}/${_Cdhcp1}" "${DESTDIR}/etc/conf.d/dhcpd"
install -Dm644 "${_gentoo_uri}/${_Cdhcp2}" "${DESTDIR}/etc/conf.d/dhcrelay"
install -Dm644 "${_gentoo_uri}/${_Cdhcp3}" "${DESTDIR}/etc/conf.d/dhcrelay6"
install -Dm755 "${_gentoo_uri}/${_Idhcp1}" "${DESTDIR}/etc/init.d/dhcpd"
install -Dm755 "${_gentoo_uri}/${_Idhcp2}" "${DESTDIR}/etc/init.d/dhcrelay"
install -Dm755 "${_gentoo_uri}/${_Idhcp2}" "${DESTDIR}/etc/init.d/dhcrelay6"
install -Dm644 "${_gentoo_uri}/${_Chost}" "${DESTDIR}/etc/conf.d/hostapd"
install -Dm755 "${_gentoo_uri}/${_Ihost}" "${DESTDIR}/etc/init.d/hostapd"
install -Dm644 "${_gentoo_uri}/${_Cip1}" "${DESTDIR}/etc/conf.d/iptables"
install -Dm755 "${_gentoo_uri}/${_Iip}" "${DESTDIR}/etc/init.d/iptables"
install -Dm644 "${_gentoo_uri}/${_Cip2}" "${DESTDIR}/etc/conf.d/ip6tables"
install -Dm755 "${_gentoo_uri}/${_Iip}" "${DESTDIR}/etc/init.d/ip6tables"
#install -Dm755 "${_gentoo_uri}/${_Ikrb1}" "${DESTDIR}/etc/init.d/krb5kadmind"
#install -Dm755 "${_gentoo_uri}/${_Ikrb2}" "${DESTDIR}/etc/init.d/krb5kdc"
#install -Dm755 "${_gentoo_uri}/${_Ikrb3}" "${DESTDIR}/etc/init.d/krb5kpropd"
#install -Dm644 "${_gentoo_uri}/${_Clight}" "${DESTDIR}/etc/conf.d/lighttpd"
#install -Dm755 "${_gentoo_uri}/${_Ilight}" "${DESTDIR}/etc/init.d/lighttpd"
install -Dm644 "${_gentoo_uri}/${_Crpc}" "${DESTDIR}/etc/conf.d/rpcbind"
install -Dm755 "${_gentoo_uri}/${_Irpc}" "${DESTDIR}/etc/init.d/rpcbind"
install -Dm644 "${_gentoo_uri}/${_Cnfs}" "${DESTDIR}/etc/conf.d/nfs"
install -Dm755 "${_gentoo_uri}/${_Infs}" "${DESTDIR}/etc/init.d/nfs"
install -Dm644 "${_gentoo_uri}/${_Cnfsc}" "${DESTDIR}/etc/conf.d/nfsclient"
install -Dm755 "${_gentoo_uri}/${_Infsc}" "${DESTDIR}/etc/init.d/nfsclient"
install -Dm755 "${_gentoo_uri}/${_Igssd}" "${DESTDIR}/etc/init.d/rpc.gssd"
install -Dm755 "${_gentoo_uri}/${_Iidmapd}" "${DESTDIR}/etc/init.d/rpc.idmapd"
install -Dm755 "${_gentoo_uri}/${_Ipipefs}" "${DESTDIR}/etc/init.d/rpc.pipefs"
install -Dm755 "${_gentoo_uri}/${_Istatd}" "${DESTDIR}/etc/init.d/rpc.statd"
install -Dm755 "${_gentoo_uri}/${_Isvcgssd}" "${DESTDIR}/etc/init.d/rpc.svcgssd"
install -Dm644 "${_gentoo_uri}/${_Contp}" "${DESTDIR}/etc/conf.d/openntpd"
install -Dm755 "${_gentoo_uri}/${_Iontp}" "${DESTDIR}/etc/init.d/openntpd"
install -Dm644 "${_gentoo_uri}/${_Cldap}" "${DESTDIR}/etc/conf.d/slapd"
install -Dm755 "${_gentoo_uri}/${_Ildap}" "${DESTDIR}/etc/init.d/slapd"
#install -Dm755 "${_gentoo_uri}/${_Islp}" "${DESTDIR}/etc/init.d/slpd"
install -Dm644 "${_gentoo_uri}/${_Cssh}" "${DESTDIR}/etc/conf.d/sshd"
install -Dm755 "${_gentoo_uri}/${_Issh}" "${DESTDIR}/etc/init.d/sshd"
install -Dm644 "${_gentoo_uri}/${_Cvpn}" "${DESTDIR}/etc/conf.d/openvpn"
install -Dm755 "${_gentoo_uri}/${_Ivpn}" "${DESTDIR}/etc/init.d/openvpn"
install -Dm755 "${_gentoo_uri}/${_Ipolip}" "${DESTDIR}/etc/init.d/polipo"
install -Dm755 "${_gentoo_uri}/${_Ipostfix}" "${DESTDIR}/etc/init.d/postfix"
install -Dm755 "${_gentoo_uri}/${_Ipriv}" "${DESTDIR}/etc/init.d/privoxy"
install -Dm644 "${_gentoo_uri}/${_Cquota}" "${DESTDIR}/etc/conf.d/quota"
install -Dm755 "${_gentoo_uri}/${_Iquota1}" "${DESTDIR}/etc/init.d/quota"
install -Dm755 "${_gentoo_uri}/${_Iquota2}" "${DESTDIR}/etc/init.d/rpc.rquotad"
install -Dm644 "${_gentoo_uri}/${_Crsync}" "${DESTDIR}/etc/conf.d/rsyncd"
install -Dm755 "${_gentoo_uri}/${_Irsync}" "${DESTDIR}/etc/init.d/rsyncd"
install -Dm644 "${_gentoo_uri}/${_Csmb}" "${DESTDIR}/etc/conf.d/samba"
install -Dm755 "${_gentoo_uri}/${_Ismb}" "${DESTDIR}/etc/init.d/samba"
install -Dm644 "${_gentoo_uri}/${_Csquid}" "${DESTDIR}/etc/conf.d/squid"
install -Dm755 "${_gentoo_uri}/${_Isquid}" "${DESTDIR}/etc/init.d/squid"
install -Dm644 "${_gentoo_uri}/${_Ctrans}" "${DESTDIR}/etc/conf.d/transmission-daemon"
install -Dm755 "${_gentoo_uri}/${_Itrans}" "${DESTDIR}/etc/init.d/transmission-daemon"
install -Dm644 "${_gentoo_uri}/${_Cufw}" "${DESTDIR}/etc/conf.d/ufw"
install -Dm755 "${_gentoo_uri}/${_Iufw}" "${DESTDIR}/etc/init.d/ufw"
install -Dm755 "${_gentoo_uri}/${_Ivsftp}" "${DESTDIR}/etc/init.d/vsftpd"
install -Dm755 "${_gentoo_uri}/${_Svsftp}" "${DESTDIR}/usr/libexec/vsftpd-checkconfig.sh"
install -Dm755 "${_gentoo_uri}/${_Iproftp}" "${DESTDIR}/etc/init.d/proftpd"
install -Dm644 "${_gentoo_uri}/${_Cvnstat}" "${DESTDIR}/etc/conf.d/vnstatd"
install -Dm755 "${_gentoo_uri}/${_Ivnstat}" "${DESTDIR}/etc/init.d/vnstatd"
install -Dm644 "${_gentoo_uri}/${_Cxinet}" "${DESTDIR}/etc/conf.d/xinetd"
install -Dm755 "${_gentoo_uri}/${_Ixinet}" "${DESTDIR}/etc/init.d/xinetd"
install -Dm644 "${_gentoo_uri}/${_Cypbind}" "${DESTDIR}/etc/conf.d/ypbind"
install -Dm755 "${_gentoo_uri}/${_Iypbind}" "${DESTDIR}/etc/init.d/ypbind"
install -Dm644 "${_gentoo_uri}/${_Iypserv1}" "${DESTDIR}/etc/conf.d/ypserv"
install -Dm755 "${_gentoo_uri}/${_Cypserv1}" "${DESTDIR}/etc/init.d/ypserv"
#install -Dm644 "${_gentoo_uri}/${_Cypserv2}" "${DESTDIR}/etc/conf.d/rpc.yppasswdd"
#install -Dm755 "${_gentoo_uri}/${_Iypserv2}" "${DESTDIR}/etc/init.d/rpc.yppasswdd"
#install -Dm644 "${_gentoo_uri}/${_Cypserv3}" "${DESTDIR}/etc/conf.d/rpc.ypxfrd"
#install -Dm755 "${_gentoo_uri}/${_Iypserv3}" "${DESTDIR}/etc/init.d/rpc.ypxfrd"
install -Dm644 "${_gentoo_uri}/${_Ctor}" "${DESTDIR}/etc/conf.d/tor"
install -Dm755 "${_gentoo_uri}/${_Itor}" "${DESTDIR}/etc/init.d/tor"
install -Dm755 "${_gentoo_uri}/${_Inginx}" "${DESTDIR}/etc/init.d/nginx"
install -Dm644 "${_gentoo_uri}/${_CNM}" "${DESTDIR}/etc/conf.d/NetworkManager"
install -Dm755 "${_gentoo_uri}/${_INM}" "${DESTDIR}/etc/init.d/NetworkManager"
install -Dm755 "${_gentoo_uri}/${_SNM}" "${DESTDIR}/etc/NetworkManager/dispatcher.d/10-openrc-status"
install -Dm644 "misc/gentoo-apache-${_apver}/init/apache2.confd" "${DESTDIR}/etc/conf.d/httpd"
install -Dm755 "misc/gentoo-apache-${_apver}/init/apache2.initd" "${DESTDIR}/etc/init.d/httpd"
install -Dm755 "misc/init.d/wicd.initd" "${DESTDIR}/etc/init.d/wicd"
install -Dm644 "misc/conf.d/syncthing.confd" "${DESTDIR}/etc/conf.d/syncthing"
install -Dm755 "misc/init.d/syncthing.initd" "${DESTDIR}/etc/init.d/syncthing"
install -Dm644 "misc/conf.d/dnsmasq.confd" "${DESTDIR}/etc/conf.d/dnsmasq"
install -Dm755 "misc/init.d/dnsmasq.init" "${DESTDIR}/etc/init.d/dnsmasq"

# comments
#
# lighttpd not installed by default due to lack of /etc/lighttpd/lighttpd.conf
# which is sourced by /etc/conf.d/lighttpd and causes error when starting services.
