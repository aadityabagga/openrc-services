#!/bin/sh
# install_openrc_video.sh

DESTDIR=$1

_gentoo_uri="gentoo-x86"

# files
_Cbumb=x11-misc/bumblebee/files/bumblebee.confd
_Ibumb=x11-misc/bumblebee/files/bumblebee.initd
_Inv=x11-drivers/nvidia-drivers/files/nvidia-smi.init
_Cnvp=x11-drivers/nvidia-drivers/files/nvidia-persistenced.conf
_Invp=x11-drivers/nvidia-drivers/files/nvidia-persistenced.init
_Iati=x11-drivers/ati-drivers/files/atieventsd.init
_Cvgl=x11-misc/virtualgl/files/vgl.confd-r1
_Ivgl=x11-misc/virtualgl/files/vgl.initd-r2
_Svgl=x11-misc/virtualgl/files/vgl-helper.sh

# install
install -Dm755 "${_gentoo_uri}/${_Inv}" "${DESTDIR}"/etc/init.d/nvidia-smi
install -Dm644 "${_gentoo_uri}/${_Cnvp}" "${DESTDIR}"/etc/conf.d/nvidia-persistenced
install -Dm755 "${_gentoo_uri}/${_Invp}" "${DESTDIR}"/etc/init.d/nvidia-persistenced
install -Dm755 "${_gentoo_uri}/${_Iati}" "${DESTDIR}"/etc/init.d/atieventsd
echo 'ATIEVENTSDOPTS=""' > "${DESTDIR}"/etc/conf.d/atieventsd
install -Dm644 "${_gentoo_uri}/${_Cvgl}" "${DESTDIR}"/etc/conf.d/vgl
install -Dm755 "${_gentoo_uri}/${_Ivgl}" "${DESTDIR}"/etc/init.d/vgl
install -Dm755 "${_gentoo_uri}/${_Svgl}" "${DESTDIR}"/usr/lib/${_Svgl}
install -d "${DESTDIR}"/var/lib/VirtualGL
install -Dm644 "${_gentoo_uri}/${_Cbumb}" "${DESTDIR}"/etc/conf.d/bumblebee
install -Dm755 "${_gentoo_uri}/${_Ibumb}" "${DESTDIR}"/etc/init.d/bumblebee
