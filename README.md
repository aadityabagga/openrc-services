# OpenRC Services

Downloads source for OpenRC services from Gentoo via cvs, and keeps track of changes using git.

Note-
If you are getting warnings about $HOME/.cvspass not present then it can be created as:

~~~~
touch $HOME/.cvspass
~~~~

## Upstream link

http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86

## Install scripts

Install scripts are based upon https://github.com/manjaro/packages-openrc

## Update

Newer version of this repo is present here https://bitbucket.org/aadityabagga/openrc-services-ng