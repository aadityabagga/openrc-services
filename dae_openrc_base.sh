#!/bin/sh
# install_openrc_base.sh

DESTDIR=$1

FILES="gentoo-x86"
_udev="udev-init-scripts"
_uver=30

FILES=(
	[_Icron]='sys-process/cronie/files/cronie-1.3-initd'
	[_Ccrypt]='sys-fs/cryptsetup/files/1.6.7-dmcrypt.confd'
	[_Icrypt]='sys-fs/cryptsetup/files/1.6.7-dmcrypt.rc'
	[_Idbus]='sys-apps/dbus/files/dbus.initd'
	[_Inscd]='sys-libs/glibc/files/nscd'
	[_Cdm]='sys-fs/lvm2/files/device-mapper.conf-1.02.22-r3'
	[_Idm1]='sys-fs/lvm2/files/device-mapper.rc-2.02.95-r2'
	[_Idm2]='sys-fs/lvm2/files/dmeventd.initd-2.02.67-r1'
	[_Clvm]='sys-fs/lvm2/files/lvm.confd-2.02.28-r2'
	[_Ilvm1]='sys-fs/lvm2/files/lvm.rc-2.02.95-r2'
	[_Ilvm2]='sys-fs/lvm2/files/lvm-monitoring.initd-2.02.67-r2'
	[_Cmdadm]='sys-fs/mdadm/files/mdadm.confd'
	[_Imdadm]='sys-fs/mdadm/files/mdadm.rc'
	[_Idhcpcd]='net-misc/dhcpcd/files/dhcpcd.initd'
	[_Cmdraid]='sys-fs/mdadm/files/mdraid.confd'
	[_Imdraid]='sys-fs/mdadm/files/mdraid.rc'
	[_Ikmod]='sys-apps/kmod/files/kmod-static-nodes-r1'
)
	#_Idm1=sys-fs/lvm2/files/device-mapper.rc-2.02.105-r2
	#_Ilvm1=sys-fs/lvm2/files/lvm.rc-2.02.105-r2
	#_Ilvm2=sys-fs/lvm2/files/lvm-monitoring.initd-2.02.105-r2
	#_Ilvm3=sys-fs/lvm2/files/lvmetad.initd-2.02.105-r2
	#_Ieudev=sys-fs/eudev/files/udev-postmount

# install
install -Dm755 "${FILES[_Icron]}" "${DESTDIR}/etc/init.d/cronie"
install -Dm755 "${FILES[_Idhcpcd]}" "${DESTDIR}/etc/init.d/dhcpcd"
install -Dm755 "${FILES[_Idbus]}" "${DESTDIR}/etc/init.d/dbus"
install -Dm644 "${FILES[_Cdm]}" "${DESTDIR}/etc/conf.d/device-mapper"
install -Dm755 "${FILES[_Idm1]}" "${DESTDIR}/etc/init.d/device-mapper"
install -Dm755 "${FILES[_Idm2]}" "${DESTDIR}/etc/init.d/dmeventd"
install -Dm644 "${FILES[_Ccrypt]}" "${DESTDIR}/etc/conf.d/dmcrypt"
install -Dm755 "${FILES[_Icrypt]}" "${DESTDIR}/etc/init.d/dmcrypt"
install -Dm755 "${FILES[_Inscd]}" "${DESTDIR}/etc/init.d/nscd"
install -Dm644 "${FILES[_Clvm]}" "${DESTDIR}/etc/conf.d/lvm"
install -Dm755 "${FILES[_Ilvm1]}" "${DESTDIR}/etc/init.d/lvm"
install -Dm755 "${FILES[_Ilvm2]}" "${DESTDIR}/etc/init.d/lvm-monitoring"
install -Dm644 "${FILES[_Cmdadm]}" "${DESTDIR}/etc/conf.d/mdadm"
install -Dm755 "${FILES[_Imdadm]}" "${DESTDIR}/etc/init.d/mdadm"
install -Dm644 "${FILES[_Cmdraid]}" "${DESTDIR}/etc/conf.d/mdraid"
install -Dm755 "${FILES[_Imdraid]}" "${DESTDIR}/etc/init.d/mdraid"
install -Dm755 "${FILES[_Ikmod]}" "${DESTDIR}/etc/init.d/kmod-static-nodes"
install -Dm644 "misc/${_udev}/conf.d/udev" "${DESTDIR}/etc/conf.d/udev"
install -Dm755 "misc/${_udev}/init.d/udev" "${DESTDIR}/etc/init.d/udev"
install -Dm644 "misc/${_udev}/conf.d/udev-settle" "${DESTDIR}/etc/conf.d/udev-settle"
install -Dm755 "misc/${_udev}/init.d/udev-settle" "${DESTDIR}/etc/init.d/udev-settle"
install -Dm644 "misc/${_udev}/conf.d/udev-trigger" "${DESTDIR}/etc/conf.d/udev-trigger"
install -Dm755 "misc/${_udev}/init.d/udev-trigger" "${DESTDIR}/etc/init.d/udev-trigger"
#install -Dm755 "${FILES}/${_Ilvm3}" "${DESTDIR}/etc/init.d/lvmetad"
#install -Dm755 "${FILES}/${_Ieudev}" "${DESTDIR}/etc/init.d/udev-postmount"

# comments
# _Ieudev (udev-postmount) got removed due to updated udev scripts.
