#!/bin/sh
# install_openrc_devel.sh

DESTDIR=$1

_gentoo_uri="gentoo-x86"

# files
_Cgit=dev-vcs/git/files/git-daemon.confd
_Igit=dev-vcs/git/files/git-daemon-r1.initd
_Cmy=dev-db/mysql-init-scripts/files/conf.d-2.0
_Imy=dev-db/mysql-init-scripts/files/init.d-2.0
_Csvn=dev-vcs/subversion/files/svnserve.confd
_Isvn=dev-vcs/subversion/files/svnserve.initd3
_CPgsql=dev-db/postgresql/files/postgresql.confd
_IPgsql=dev-db/postgresql/files/postgresql.init-9.3
_Iphp=dev-lang/php/files/php-fpm-r4.init

# install
install -Dm644 "${_gentoo_uri}/${_Cgit}" "${DESTDIR}/etc/conf.d/git-daemon"
install -Dm755 "${_gentoo_uri}/${_Igit}" "${DESTDIR}/etc/init.d/git-daemon"
install -Dm644 "${_gentoo_uri}/${_Cmy}" "${DESTDIR}/etc/conf.d/mysqld"
install -Dm755 "${_gentoo_uri}/${_Imy}" "${DESTDIR}/etc/init.d/mysqld"
install -Dm755 "${_gentoo_uri}/${_Iphp}" "${DESTDIR}/etc/init.d/php-fpm"
install -Dm644 "${_gentoo_uri}/${_CPgsql}" "${DESTDIR}/etc/conf.d/postgresql"
install -Dm755 "${_gentoo_uri}/${_IPgsql}" "${DESTDIR}/etc/init.d/postgresql"
install -Dm644 "${_gentoo_uri}/${_Csvn}" "${DESTDIR}/etc/conf.d/svn"
install -Dm755 "${_gentoo_uri}/${_Isvn}" "${DESTDIR}/etc/init.d/svn"
