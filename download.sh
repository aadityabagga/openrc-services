#!/bin/bash
# download.sh: downloads openrc scripts from gentoo sources and keeps them upto date using cvs

# Copyright (C) 2015 Aaditya Bagga <aaditya_gnulinux@zoho.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed WITHOUT ANY WARRANTY;
# without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

# CATEGORIES are sorted alphabetically
# PROGRAMS in each CATEGORY are present as separate arrays

# make CATEGORIES an associative array, so that its items can be accessed by name
declare -A CATEGORIES

CATEGORIES=(
	[app_admin]='app-admin'
	[app_antivirus]='app-antivirus'
	[app_crypt]='app-crypt'
	[app_emulation]='app-emulation'
	[app_misc]='app-misc'
	[dev_db]='dev-db'
	[dev_lang]='dev-lang'
	[dev_libs]='dev-libs'
	[dev_vcs]='dev-vcs'
	[mail_mta]='mail-mta'
	[media_gfx]='media-gfx'
	[media_sound]='media-sound'
	[net_analyzer]='net-analyzer'
	[net_dns]='net-dns'
	[net_firewall]='net-firewall'
	[net_fs]='net-fs'
	[net_ftp]='net-ftp'
	[net_im]='net-im'
	[net_libs]='net-libs'
	[net_misc]='net-misc'
	[net_nds]='net-nds'
	[net_p2p]='net-p2p'
	[net_print]='net-print'
	[net_proxy]='net-proxy'
	[net_wireless]='net-wireless'
	[sci_misc]='sci-misc'
	[sys_apps]='sys-apps'
	[sys_auth]='sys-auth'
	[sys_fs]='sys-fs'
	[sys_libs]='sys-libs'
	[sys_power]='sys-power'
	[sys_process]='sys-process'
	[www_servers]='www-servers'
	[x11_apps]='x11-apps'
	[x11_base]='x11-base'
	[x11_drivers]='x11-drivers'
	[x11_misc]='x11-misc'
)

# We cannot store a variable dev-db, so we use associative array above
# and set arrays like dev_db

app_admin=(cgmanager metalog rsyslog salt sysklogd syslog-ng ulogd)
app_antivirus=(clamav)
app_crypt=(mit-krb5)
app_emulation=(libvirt virtualbox-guest-additions xe-guest-utilities)
app_misc=(lirc)
dev_db=(mysql-init-scripts postgresql)
dev_lang=(php)
dev_libs=(cyrus-sasl)
dev_vcs=(git subversion)
mail_mta=(postfix sendmail)
media_gfx=(sane-backends)
media_sound=(alsa-utils mpd pulseaudio)
net_analyzer=(vnstat net-snmp)
net_dns=(bind)
net_firewall=(iptables ufw)
net_fs=(autofs nfs-utils samba)
net_ftp=(proftpd vsftpd)
net_im=(bitlbee)
net_libs=(openslp)
net_misc=(connman dhcp dhcpcd dnsmasq networkmanager ntp openntpd openssh openvpn rsync teamviewer tor)
net_nds=(openldap rpcbind ypbind ypserv)
net_p2p=(transmission)
net_print=(cups)
net_proxy=(polipo privoxy squid)
net_wireless=(bluez hostapd wpa_supplicant)
sci_misc=(boinc)
sys_apps=(dbus haveged hdparm kmod lm_sensors xinetd)
sys_auth=(consolekit)
sys_fs=(cryptsetup eudev fuse lvm2 mdadm quota)
sys_libs=(glibc gpm)
sys_power=(acpid cpupower thermald)
sys_process=(anacron at cronie dcron fcron)
www_servers=(lighttpd nginx)
x11_apps=(xinit)
x11_base=(xorg-server)
x11_drivers=(nvidia-drivers ati-drivers)
x11_misc=(virtualgl bumblebee)

# missing
# OPENRC_BASE: inetutils
# OPENRC_DESKTOP: avahi
# OPENRC_MISC: zfs
# OPENRC_NET: apache wicd syncthing

# Test
if [ "$1" = -t ]; then
	# Searching in the indices of CATEGORIES
	for i in ${!CATEGORIES[*]}; do
		# Now we get the actual category
		k=${CATEGORIES[$i]}
		# Now we need to evaluate the array pointed by CATEGORY index
		eval "j=\${$i[*]}"
		# Print programs in each category
		for l in $j; do
			echo "$k: $l"
		done
	done
	exit 0
elif [ "$1" = -p ]; then
	# Second argument is the package category
	cat="$2"
	# Third argument is the package name
	package="$3"
	# Checkout the source
	cvs -d :pserver:anonymous@anoncvs.gentoo.org:/var/cvsroot co -l "gentoo-x86/$cat/$package/files"
	# Exit with status
	exit $?
fi

log="logs/download.log"
# Check for log directory
[ ! -d logs ] && mkdir logs
# Initialize log with date
date +"%F_%T" >> $log

# Searching in the indices of CATEGORIES
for i in ${!CATEGORIES[*]}; do
	# Now we get the actual category
	k=${CATEGORIES[$i]}
	# Now we need to evaluate the array pointed by CATEGORY index
	eval "j=\${$i[*]}"
	for l in $j; do
		# Checkout the source and write to log
		cvs -d :pserver:anonymous@anoncvs.gentoo.org:/var/cvsroot co -l "gentoo-x86/$k/$l/files" 2>&1 | tee -a "$log"
	done
done

# Some special downloads
cvs -d :pserver:anonymous@anoncvs.gentoo.org:/var/cvsroot co -l "gentoo-x86/app-admin/rsyslog/files/8-stable" 2>&1 | tee -a "$log"
cvs -d :pserver:anonymous@anoncvs.gentoo.org:/var/cvsroot co -l "gentoo-x86/app-admin/syslog-ng/files/3.6" 2>&1 | tee -a "$log"
cvs -d :pserver:anonymous@anoncvs.gentoo.org:/var/cvsroot co -l "gentoo-x86/net-fs/samba/files/4.2" 2>&1 | tee -a "$log"

exit $?
