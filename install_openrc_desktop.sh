#!/bin/sh
# install_openrc_desktop.sh

DESTDIR=$1

_gentoo_uri="gentoo-x86"

# files
_Cacpi=sys-power/acpid/files/acpid-2.0.16-conf.d
_Iacpi=sys-power/acpid/files/acpid-2.0.16-init.d
_Calsa=media-sound/alsa-utils/files/alsasound.confd-r4
_Ialsa=media-sound/alsa-utils/files/alsasound.initd-r6
_Ick=sys-auth/consolekit/files/consolekit-0.2.rc
_Icgm=app-admin/cgmanager/files/cgmanager.initd
_Icgp=app-admin/cgmanager/files/cgproxy.initd
_Cxdm=x11-base/xorg-server/files/xdm.confd-4
_Ixdm1=x11-base/xorg-server/files/xdm.initd-11
_Ixdm2=x11-base/xorg-server/files/xdm-setup.initd-1
_Sxdm=x11-apps/xinit/files/startDM.sh
_Cgpm=sys-libs/gpm/files/gpm.conf.d
_Igpm=sys-libs/gpm/files/gpm.rc6-2
_Cblue=net-wireless/bluez/files/rfcomm-conf.d
_Iblue1=net-wireless/bluez/files/rfcomm-init.d-r2
_Iblue2=net-wireless/bluez/files/bluetooth-init.d-r3
_Cwpa=net-wireless/wpa_supplicant/files/wpa_supplicant-conf.d
_Iwpa=net-wireless/wpa_supplicant/files/wpa_supplicant-init.d
_Swpa=net-wireless/wpa_supplicant/files/wpa_cli.sh

# install
install -Dm644 "${_gentoo_uri}/${_Cacpi}" "${DESTDIR}/etc/conf.d/acpid"
install -Dm755 "${_gentoo_uri}/${_Iacpi}" "${DESTDIR}/etc/init.d/acpid"
install -Dm644 "${_gentoo_uri}/${_Calsa}" "${DESTDIR}/etc/conf.d/alsasound"
install -Dm755 "${_gentoo_uri}/${_Ialsa}" "${DESTDIR}/etc/init.d/alsasound"
install -Dm755 "${_gentoo_uri}/${_Ick}" "$DESTDIR/etc/init.d/consolekit"
install -Dm755 "${_gentoo_uri}/${_Icgm}" "$DESTDIR/etc/init.d/cgmanager"
install -Dm755 "${_gentoo_uri}/${_Icgp}" "$DESTDIR/etc/init.d/cgproxy"
install -Dm644 "${_gentoo_uri}/${_Cxdm}" "${DESTDIR}/etc/conf.d/xdm"
install -Dm755 "${_gentoo_uri}/${_Ixdm1}" "${DESTDIR}/etc/init.d/xdm"
install -Dm755 "${_gentoo_uri}/${_Ixdm2}" "${DESTDIR}/etc/init.d/xdm-setup"
install -Dm755 "${_gentoo_uri}/${_Sxdm}" "${DESTDIR}/etc/X11/startDM.sh"
install -Dm644 "${_gentoo_uri}/${_Cgpm}" "${DESTDIR}/etc/conf.d/gpm"
install -Dm755 "${_gentoo_uri}/${_Igpm}" "${DESTDIR}/etc/init.d/gpm"
install -Dm644 "${_gentoo_uri}/${_Cblue}" "${DESTDIR}/etc/conf.d/rfcomm"
install -Dm755 "${_gentoo_uri}/${_Iblue1}" "${DESTDIR}/etc/init.d/rfcomm"
install -Dm755 "${_gentoo_uri}/${_Iblue2}" "${DESTDIR}/etc/init.d/bluetooth"
install -Dm644 "${_gentoo_uri}/${_Cwpa}" "${DESTDIR}/etc/conf.d/wpa_supplicant"
install -Dm755 "${_gentoo_uri}/${_Iwpa}" "${DESTDIR}/etc/init.d/wpa_supplicant"
install -Dm755 "${_gentoo_uri}/${_Swpa}" "${DESTDIR}/etc/wpa_supplicant/wpa_cli.sh"
install -Dm755 "misc/init.d/avahi-daemon" "${DESTDIR}/etc/init.d/avahi-daemon"
install -Dm755 "misc/init.d/avahi-daemon" "${DESTDIR}/etc/init.d/avahi-daemon"
install -Dm755 "misc/init.d/tlp-init.openrc-r2" "${DESTDIR}/etc/init.d/tlp"
