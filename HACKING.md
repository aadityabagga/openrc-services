### Source list

The list of services fetched upstream can be found in [source_list.sh](source_list.sh).

Other miscellaneous sources can be found under [misc](misc).

### Installing

The services are roughly categorized according to their function. For example,
 network services are installed via [install_openrc_net.sh](install_openrc_net.sh).

The installed files are patched (fixed) as needed for distribution.

[install.sh](install.sh) serves as overall makefile.
