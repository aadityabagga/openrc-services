#!/bin/sh
# install_openrc_misc.sh

DESTDIR=$1

_gentoo_uri="gentoo-x86"

# files
_Iacron=sys-process/anacron/files/anacron.rc6
_Ifrcon=sys-process/fcron/files/fcron.init.3
_Crsysl=app-admin/rsyslog/files/8-stable/rsyslog.confd
_Irsysl=app-admin/rsyslog/files/8-stable/rsyslog.initd
_Csane=media-gfx/sane-backends/files/saned.confd
_Isane=media-gfx/sane-backends/files/saned.initd
_Ifuse=sys-fs/fuse/files/fuse.init
_Cmeta=app-admin/metalog/files/metalog.confd
_Imeta=app-admin/metalog/files/metalog.initd
_Csyslog=app-admin/syslog-ng/files/3.6/syslog-ng.confd
_Isyslog=app-admin/syslog-ng/files/3.6/syslog-ng.rc6
_Clirc1=app-misc/lirc/files/lircd.conf.4
_Clirc2=app-misc/lirc/files/irexec-confd
_Ilirc1=app-misc/lirc/files/lircd-0.8.6-r2
_Ilirc2=app-misc/lirc/files/irexec-initd-0.8.6-r2
_Ilirc3=app-misc/lirc/files/lircmd
_Csens=sys-apps/lm_sensors/files/sensord-conf.d
_Isens1=sys-apps/lm_sensors/files/sensord-4-init.d
_Isens2=sys-apps/lm_sensors/files/fancontrol-init.d-2
_Isens3=sys-apps/lm_sensors/files/lm_sensors-3-init.d
_Ccpu=sys-power/cpupower/files/conf.d-r2
_Icpu=sys-power/cpupower/files/init.d-r4
_Cntp1=net-misc/ntp/files/ntpd.confd
_Cntp2=net-misc/ntp/files/ntp-client.confd
_Cntp3=net-misc/ntp/files/sntp.confd
_Intp1=net-misc/ntp/files/ntpd.rc-r1
_Intp2=net-misc/ntp/files/ntp-client.rc
_Intp3=net-misc/ntp/files/sntp.rc
_Icups=net-print/cups/files/cupsd.init.d-r1
_Ccon=net-misc/connman/files/connman.confd
_Icon=net-misc/connman/files/connman.initd2
_Chaveg=sys-apps/haveged/files/haveged-conf.d
_Ihaveg=sys-apps/haveged/files/haveged-init.d.3
_Csalt_mtr=app-admin/salt/files/master-confd-1
_Isalt_mtr=app-admin/salt/files/master-initd-4
_Csalt_min=app-admin/salt/files/minion-confd-1
_Isalt_min=app-admin/salt/files/minion-initd-4
_Csalt_sync=app-admin/salt/files/syndic-confd-1
_Isalt_sync=app-admin/salt/files/syndic-initd-4
_Impd=media-sound/mpd/files/mpd2.init
_Chdparm=sys-apps/hdparm/files/hdparm-conf.d.3
_Ihdparm=sys-apps/hdparm/files/hdparm-init-8
_Cbit=net-im/bitlbee/files/bitlbee.confd
_Ibit=net-im/bitlbee/files/bitlbee.initd
_Itherm=sys-power/thermald/files/thermald
_Ixe=app-emulation/xe-guest-utilities/files/xe-daemon.initd
_Ivbox=app-emulation/virtualbox-guest-additions/files/virtualbox-guest-additions-8.initd
_CClam=app-antivirus/clamav/files/clamd.conf-r1
_IClam=app-antivirus/clamav/files/clamd.initd-r6
_CBoinc=sci-misc/boinc/files/boinc.conf
_IBoinc=sci-misc/boinc/files/boinc.init
_CAt=sys-process/at/files/atd.confd
_IAt=sys-process/at/files/atd.rc8
_Clibvirt=app-emulation/libvirt/files/libvirtd.confd-r4
_Ilibvirt=app-emulation/libvirt/files/libvirtd.init-r14
_Ivirtlock=app-emulation/libvirt/files/virtlockd.init-r1
_CPulseAudio=media-sound/pulseaudio/files/pulseaudio.conf.d
_IPulseAudio=media-sound/pulseaudio/files/pulseaudio.init.d-5

# install
install -Dm644 "${_gentoo_uri}/${_Cbit}" "${DESTDIR}/etc/conf.d/bitlbee"
install -Dm755 "${_gentoo_uri}/${_Ibit}" "${DESTDIR}/etc/init.d/bitlbee"
install -Dm644 "${_gentoo_uri}/${_Ccpu}" "${DESTDIR}/etc/conf.d/cpupower"
install -Dm755 "${_gentoo_uri}/${_Icpu}" "${DESTDIR}/etc/init.d/cpupower"
install -Dm644 "${_gentoo_uri}/${_Ccon}" "${DESTDIR}/etc/conf.d/connman"
install -Dm755 "${_gentoo_uri}/${_Icon}" "${DESTDIR}/etc/init.d/connman"
install -Dm755 "${_gentoo_uri}/${_Icups}" "${DESTDIR}/etc/init.d/cupsd"
install -Dm755 "${_gentoo_uri}/${_Iacron}" "${DESTDIR}/etc/init.d/anacron"
install -Dm755 "${_gentoo_uri}/${_Ifrcon}" "${DESTDIR}/etc/init.d/fcron"
install -Dm644 "${_gentoo_uri}/${_Cntp1}" "${DESTDIR}/etc/conf.d/ntpd"
install -Dm755 "${_gentoo_uri}/${_Intp1}" "${DESTDIR}/etc/init.d/ntpd"
install -Dm644 "${_gentoo_uri}/${_Cntp2}" "${DESTDIR}/etc/conf.d/ntp-client"
install -Dm755 "${_gentoo_uri}/${_Intp2}" "${DESTDIR}/etc/init.d/ntp-client"
install -Dm644 "${_gentoo_uri}/${_Cntp3}" "${DESTDIR}/etc/conf.d/sntp"
install -Dm755 "${_gentoo_uri}/${_Intp3}" "${DESTDIR}/etc/init.d/sntp"
install -Dm644 "${_gentoo_uri}/${_Crsysl}" "${DESTDIR}/etc/conf.d/rsyslog"
install -Dm755 "${_gentoo_uri}/${_Irsysl}" "${DESTDIR}/etc/init.d/rsyslog"
install -Dm644 "${_gentoo_uri}/${_Csane}" "${DESTDIR}/etc/conf.d/saned"
install -Dm755 "${_gentoo_uri}/${_Isane}" "${DESTDIR}/etc/init.d/saned"
install -Dm755 "${_gentoo_uri}/${_Ifuse}" "${DESTDIR}/etc/init.d/fuse"
install -Dm644 "${_gentoo_uri}/${_Cmeta}" "${DESTDIR}/etc/conf.d/metalog"
install -Dm755 "${_gentoo_uri}/${_Imeta}" "${DESTDIR}/etc/init.d/metalog"
install -Dm644 "${_gentoo_uri}/${_Csyslog}" "${DESTDIR}/etc/conf.d/syslog-ng"
install -Dm755 "${_gentoo_uri}/${_Isyslog}" "${DESTDIR}/etc/init.d/syslog-ng"
install -Dm644 "${_gentoo_uri}/${_Csens}" "${DESTDIR}/etc/conf.d/sensord"
install -Dm755 "${_gentoo_uri}/${_Isens1}" "${DESTDIR}/etc/init.d/sensord"
install -Dm755 "${_gentoo_uri}/${_Isens2}" "${DESTDIR}/etc/init.d/fancontrol"
install -Dm755 "${_gentoo_uri}/${_Isens3}" "${DESTDIR}/etc/init.d/lm_sensors"
install -Dm644 "${_gentoo_uri}/${_Clirc1}" "${DESTDIR}/etc/conf.d/lircd"
install -Dm755 "${_gentoo_uri}/${_Ilirc1}" "${DESTDIR}/etc/init.d/lircd"
install -Dm644 "${_gentoo_uri}/${_Clirc2}" "${DESTDIR}/etc/conf.d/irexec"
install -Dm755 "${_gentoo_uri}/${_Ilirc2}" "${DESTDIR}/etc/init.d/irexec"
install -Dm755 "${_gentoo_uri}/${_Ilirc3}" "${DESTDIR}/etc/init.d/lircmd"
install -Dm644 "${_gentoo_uri}/${_Chaveg}" "${DESTDIR}/etc/conf.d/haveged"
install -Dm755 "${_gentoo_uri}/${_Ihaveg}" "${DESTDIR}/etc/init.d/haveged"
install -Dm644 "${_gentoo_uri}/${_Csalt_mtr}" "${DESTDIR}/etc/conf.d/salt-master"
install -Dm755 "${_gentoo_uri}/${_Isalt_mtr}" "${DESTDIR}/etc/init.d/salt-master"
install -Dm644 "${_gentoo_uri}/${_Csalt_min}" "${DESTDIR}/etc/conf.d/salt-minion"
install -Dm755 "${_gentoo_uri}/${_Isalt_min}" "${DESTDIR}/etc/init.d/salt-minion"
install -Dm644 "${_gentoo_uri}/${_Csalt_sync}" "${DESTDIR}/etc/conf.d/salt-syncdic"
install -Dm755 "${_gentoo_uri}/${_Isalt_sync}" "${DESTDIR}/etc/init.d/salt-syncdic"
install -Dm755 "${_gentoo_uri}/${_Impd}" "${DESTDIR}/etc/init.d/mpd"
install -Dm644 "${_gentoo_uri}/${_Chdparm}" "${DESTDIR}/etc/conf.d/hdparm"
install -Dm755 "${_gentoo_uri}/${_Ihdparm}" "${DESTDIR}/etc/init.d/hdparm"
install -Dm755 "${_gentoo_uri}/${_Itherm}" "${DESTDIR}/etc/init.d/thermald"
install -Dm755 "${_gentoo_uri}/${_Ixe}" "${DESTDIR}/etc/init.d/xe-daemon"
install -Dm755 "${_gentoo_uri}/${_Ivbox}" "${DESTDIR}/etc/init.d/vboxservice"
install -Dm644 "${_gentoo_uri}/${_CClam}" "${DESTDIR}/etc/conf.d/clamd"
install -Dm755 "${_gentoo_uri}/${_IClam}" "${DESTDIR}/etc/init.d/clamd"
install -Dm644 "${_gentoo_uri}/${_CBoinc}" "${DESTDIR}/etc/conf.d/boinc"
install -Dm755 "${_gentoo_uri}/${_IBoinc}" "${DESTDIR}/etc/init.d/boinc"
install -Dm644 "${_gentoo_uri}/${_CAt}" "${DESTDIR}/etc/conf.d/atd"
install -Dm755 "${_gentoo_uri}/${_IAt}" "${DESTDIR}/etc/init.d/atd"
install -Dm644 "${_gentoo_uri}/${_Clibvirt}" "${DESTDIR}/etc/conf.d/libvirtd"
install -Dm755 "${_gentoo_uri}/${_Ilibvirt}" "${DESTDIR}/etc/init.d/libvirtd"
install -Dm755 "${_gentoo_uri}/${_Ivirtlock}" "${DESTDIR}/etc/init.d/virtlockd"
install -Dm644 "${_gentoo_uri}/${_CPulseAudio}" "${DESTDIR}/etc/conf.d/pulseaudio"
install -Dm755 "${_gentoo_uri}/${_IPulseAudio}" "${DESTDIR}/etc/init.d/pulseaudio"
install -Dm755 "misc/init.d/zfs.initd" "${DESTDIR}/etc/init.d/zfs"
